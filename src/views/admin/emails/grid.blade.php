@extends('admin::layouts.app')

@section('content')
<div class="container-fluid grid">
    <div class="row pt-2 pb-2">
        <div class="col-sm-9"><h4 class="page-title">Transactional Email Templates</h4></div>
        <div class="col-sm-3">
            <div class="btn-group float-sm-right">
                @if(Auth::guard('admin')->user()->role->isAvailable('admin.emails.new'))
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="setLocation('{{ route('admin.emails.new') }}')">
                    <i class="fa fa-plus-circle"></i>
                    <span>Add New Template</span>
                </button>
                @endif
                @if(Auth::guard('admin')->user()->role->isAvailable('admin.emails.resync'))
                <button type="button" class="btn btn-success waves-effect waves-light" onclick="setLocation('{{ route('admin.emails.resync') }}')">
                    <i class="fa fa-refresh"></i>
                    <span>Re-Sync</span>
                </button>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Total {{ count($templates) }} records found</h5>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="align-top">
                                        <strong>Name</strong>
                                    </th>
                                    <th class="align-top">
                                        <strong>Slug</strong>
                                    </th>
                                    <th class="align-top">
                                        <strong>Subject</strong>
                                    </th>
                                    <th class="align-top" width="10%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            @if(count($templates))
                                @foreach ($templates as $template)
                                <tr>
                                    <td style="white-space: normal;">{{ $template['name'] }}</td>
                                    <td style="white-space: normal;">{{ $template['slug'] }}</td>
                                    <td style="white-space: normal;">{{ $template['subject'] }}</td>
                                    <td>
                                        @if(Auth::guard('admin')->user()->role->isAvailable('admin.emails.edit'))
                                        <button type="button" class="btn btn-primary waves-effect waves-light" 
                                            onclick="setLocation('{{ route('admin.emails.edit', ['name' => $template['name']]) }}')">
                                            <i class="fa fa-pencil"></i>
                                            <span></span>
                                        </button>
                                        @endif
                                        @if(Auth::guard('admin')->user()->role->isAvailable('admin.emails.delete'))
                                        <button type="button" class="btn btn-danger waves-effect waves-light" 
                                            onclick="confirmSetLocation('{{ route('admin.emails.delete', ['name' => $template['name']]) }}')">
                                            <i class="fa fa-trash-o"></i>
                                            <span></span>
                                        </button>
                                        @endif
                                        @if(Auth::guard('admin')->user()->role->isAvailable('admin.emails.view'))
                                        <button type="button" class="btn btn-warning waves-effect waves-light" 
                                            onclick="window.open('{{ route('admin.emails.view', ['name' => $template['name']]) }}')">
                                            <i class="fa fa-eye"></i>
                                            <span></span>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="2">There no items found</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@includeIf('admin::widget.confirmation')

@endsection