@extends('admin::layouts.app')

@section('content')
<div class="container-fluid form">
    <div class="row pt-2 pb-2">
        <div class="col-sm-12">
            <div class="btn-group float-sm-right">
                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="window.history.go(-1)">
                    <i class="fa fa-arrow-circle-o-left"></i>
                    <span>Back</span>
                </button>
                @if(Auth::guard('admin')->user()->role->isAvailable('admin.emails.save'))
                <button type="button" class="btn btn-success waves-effect waves-light" onclick="$('#edit-form').submit()">
                    <i class="fa fa-check-circle"></i>
                    <span>Save</span>
                </button>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header text-uppercase">{{ !empty($template) ? 'Edit Email Template' : 'New Email Template' }}</div>
                <div class="card-body">
                    <form id="edit-form" action="{{ route('admin.emails.save') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="old_name" class="form-control" value="{{ !empty($template) ? $template['name'] : '' }}" />
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" class="form-control" value="{{ !empty($template) ? $template['name'] : '' }}" required="required" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Subject</label>
                            <div class="col-sm-9">
                                <input type="text" name="subject" class="form-control" value="{{ !empty($template) ? $template['subject'] : '' }}" required="required" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Content</label>
                            <div class="col-sm-9">
                                <textarea rows="20" class="form-control" name="code" required="required">{{ !empty($template) ? $template['code'] : '' }}</textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
// <![CDATA[
jQuery(function($) {
    $("#edit-form").validate();
});
// ]]>
</script>

@endsection