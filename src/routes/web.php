<?php

Route::prefix(env('ADMIN_PATH', 'admin'))->group(function() {
    Route::prefix('emails')->group(function() {
        Route::get('/', '\WFN\Emails\Http\Controllers\Admin\EmailController@index')->name('admin.emails');
        Route::get('/edit/{name}', '\WFN\Emails\Http\Controllers\Admin\EmailController@edit')->name('admin.emails.edit');
        Route::get('/view/{name}', '\WFN\Emails\Http\Controllers\Admin\EmailController@view')->name('admin.emails.view');
        Route::get('/new', '\WFN\Emails\Http\Controllers\Admin\EmailController@new')->name('admin.emails.new');
        Route::get('/resync', '\WFN\Emails\Http\Controllers\Admin\EmailController@resync')->name('admin.emails.resync');
        Route::post('/save', '\WFN\Emails\Http\Controllers\Admin\EmailController@save')->name('admin.emails.save');
        Route::get('/delete/{name}', '\WFN\Emails\Http\Controllers\Admin\EmailController@delete')->name('admin.emails.delete');
    });
});
