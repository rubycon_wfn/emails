<?php
namespace WFN\Emails\Providers;

use WFN\Admin\Providers\ServiceProvider as WFNServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;
use WFN\Emails\Model\Transport\Mandrill;

class ServiceProvider extends WFNServiceProvider
{

    public function register()
    {
        $this->app->booting(function() {
            $loader = AliasLoader::getInstance();
            $loader->alias('MandrillMail', Mandrill::class);
        });
        
        $this->app->singleton(Mandrill::class, function ($app) {
            return new Mandrill($app['config']->get('mail', []));
        });
    }
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $basePath = realpath(__DIR__ . '/..');
        Route::middleware('web')->group($basePath . '/routes/web.php');
        
        $this->loadViewsFrom($basePath . '/views', 'emails');

        $this->mergeConfigFrom($basePath . '/Config/settings.php', 'settings');
        $this->mergeConfigFrom($basePath . '/Config/adminNavigation.php', 'adminNavigation');
        $this->mergeConfigFrom($basePath . '/Config/emailAdditionalGlobalFields.php', 'emailAdditionalGlobalFields');
    }

}
