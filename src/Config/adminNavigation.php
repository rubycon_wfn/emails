<?php
return [
    'emails' => [
        'route' => 'admin.emails',
        'icon'  => 'icon-envelope',
        'label' => 'Manage Emails',
    ],
];