<?php

return [
    'email' => [
        'label' => 'Email',
        'fields' => [
            'from_email' => [
                'label' => 'From Email Address',
                'type'  => 'text',
            ],
            'from_name' => [
                'label' => 'From Name',
                'type'  => 'text',
            ],
            'header_logo' => [
                'label' => 'Header Logo',
                'type'  => 'image',
            ],
            'header' => [
                'label' => 'Header',
                'type'  => 'textarea',
            ],
            'footer' => [
                'label' => 'Footer',
                'type'  => 'textarea',
            ],
        ],
    ],
];

    