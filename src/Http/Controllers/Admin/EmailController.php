<?php
namespace WFN\Emails\Http\Controllers\Admin;

use Illuminate\Http\Request;

class EmailController extends \WFN\Admin\Http\Controllers\Controller
{

    public function __construct()
    {
        $this->api = new \WFN\Emails\Model\Api();
        parent::__construct();
    }

    public function index()
    {
        $templates = $this->api->getTemplatesList();
        return view('emails::admin.emails.grid', compact('templates'));
    }

    public function edit($name)
    {
        try {
            $template = $this->api->getTemplateData($name);
            return view('emails::admin.emails.form', compact('template'));
        } catch (\Exception $e) {
            \Alert::addError($e->getMessage());
            return redirect()->back();
        }
    }

    public function view($name)
    {
        try {
            $template = $this->api->getTemplateData($name);
            return $template['publish_code'];
        } catch (\Exception $e) {
            \Alert::addError($e->getMessage());
            return redirect()->back();
        }
    }

    public function new()
    {
        return view('emails::admin.emails.form');
    }

    public function save(Request $request)
    {
        try {
            $this->api->saveTemplate($request->all());
            \Alert::addSuccess('Email template has been saved');
        } catch (\Exception $e) {
            \Alert::addError($e->getMessage());
        }
        return redirect()->route('admin.emails');
    }

    public function delete($name)
    {
        try {
            $this->api->deleteTemplate($name);
            \Alert::addSuccess('Email template has been deleted');
        } catch (\Exception $e) {
            \Alert::addError($e->getMessage());
        }
        return redirect()->back();
    }

    public function resync()
    {
        list($success, $errors) = $this->api->resync();
        \Alert::addSuccess($success . ' email template(s) succesfully synced');
        \Alert::addError($errors . ' email template(s) not synced');
        return redirect()->back();
    }

}
