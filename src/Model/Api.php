<?php

namespace WFN\Emails\Model;

use GuzzleHttp\Client;

class Api
{
    const MADRILL_API_ENDPOINT = 'https://mandrillapp.com/api/1.0';

    const CONTENT_AREA_SEPARATOR = '<!-- MAIN CONTENT AREA -->';

    public function __construct()
    {
        $this->client = new Client();
        $this->key = env('MANDRILL_SECRET');
    }

    public function getTemplatesList()
    {
        try {
            $response = $this->client->post(self::MADRILL_API_ENDPOINT . '/templates/list.json', [
                'body' => json_encode([
                    'key' => $this->key,
                ])
            ]);
            $templates = json_decode($response->getBody(), true);
        } catch(\Exception $e) {
            $templates = [];
        }
        return $templates;
    }

    public function getTemplateData($name)
    {
        $response = $this->client->post(self::MADRILL_API_ENDPOINT . '/templates/info.json', [
            'body' => json_encode([
                'key'  => $this->key,
                'name' => $name
            ])
        ]);
        $template = json_decode($response->getBody(), true);
        $template['code'] = $this->_prepareTemplateCode($template['code']);

        return $template;
    }

    public function saveTemplate($data)
    {
        $apiEndpointPath = '/templates/update.json';

        try {
            if(optional($data)['name'] != optional($data)['old_name']) {
                $this->deleteTemplate(optional($data)['old_name']);
            }
        } catch (\Exception $e) {
            // pass
        }

        try {
            $this->getTemplateData(optional($data)['name']);
        } catch (\Exception $e) {
            $apiEndpointPath = '/templates/add.json';
        }
        
        return $this->client->post(self::MADRILL_API_ENDPOINT . $apiEndpointPath, [
            'body' => json_encode($this->_getPreparedTemplateData(
                optional($data)['name'],
                optional($data)['subject'],
                optional($data)['code']
            )),
        ]);
    }

    public function deleteTemplate($name)
    {
        $this->client->post(self::MADRILL_API_ENDPOINT . '/templates/delete.json', [
            'body' => json_encode([
                'key'  => $this->key,
                'name' => $name
            ])
        ]);
    }

    public function resync()
    {
        $success = 0;
        $errors  = 0;
        foreach ($this->getTemplatesList() as $template) {
            try {
                $template['code'] = $this->_prepareTemplateCode($template['code']);

                $this->client->post(self::MADRILL_API_ENDPOINT . '/templates/update.json', [
                    'body' => json_encode($this->_getPreparedTemplateData(
                        $template['name'],
                        $template['subject'],
                        $template['code']
                    )),
                ]);
                $success++;
            } catch (\Exception $e) {
                $errors++;
            }
        }
        return [$success, $errors];
    }

    protected function _getPreparedTemplateData($name, $subject, $code)
    {
        return [
            'key'        => $this->key,
            'name'       => $name,
            'from_email' => \Settings::getConfigValue('email/from_email'),
            'from_name'  => \Settings::getConfigValue('email/from_name'),
            'subject'    => $subject,
            'code'       => \Settings::getConfigValue('email/header') . PHP_EOL . self::CONTENT_AREA_SEPARATOR . PHP_EOL . $code . PHP_EOL . self::CONTENT_AREA_SEPARATOR . PHP_EOL . \Settings::getConfigValue('email/footer'),
            'publish'    => true,
        ];
    }

    protected function _prepareTemplateCode($code)
    {
        $code = explode(self::CONTENT_AREA_SEPARATOR, $code);
        if(isset($code[1])) {
            $code = trim($code[1], PHP_EOL);
        } else {
            $code = $code[0];
        }
        return $code;
    }


}