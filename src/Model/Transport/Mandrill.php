<?php

namespace WFN\Emails\Model\Transport;

use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Arr;
use Settings;

class Mandrill
{

    const MANDRILL_ENDPOINT = 'https://mandrillapp.com/api/1.0/messages/send-template.json';
    
    protected $client;

    protected $key;

    public function __construct($config)
    {
        $this->client = new HttpClient(Arr::add(
            $config['guzzle'] ?? [], 'connect_timeout', 60
        ));
        $this->key    = env('MANDRILL_SECRET');
    }

    protected function send($templateId, $to, $params, $bcc = false)
    {
        $preparedParams = [];
        foreach($params as $key => $value) {
            $preparedParams[] = [
                'name'    => $key,
                'content' => $value,
            ];
        }

        $preparedParams = array_merge($preparedParams, $this->_getGlobalParams());

        try {
            $response = $this->client->post(static::MANDRILL_ENDPOINT, [
                'body' => json_encode([
                    'key'              => $this->key,
                    'template_name'    => $templateId,
                    'template_content' => $preparedParams,
                    'message'          => [
                        'to' => [
                            ['email' => $to,]
                        ],
                        'global_merge_vars' => $preparedParams,
                        'bcc_address'       => $bcc,
                    ],
                    'async'            => true,
                ]),
            ]);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    protected function _getGlobalParams()
    {
        $images = ['header_logo'];
        $globalParams = [];
        
        foreach($images as $key) {
            $globalParams[] = [
                'name'    => $key,
                'content' => url(Settings::getConfigValueUrl('email/' . $key)),
            ];
        }

        $globalParams[] = [
            'name'    => 'year',
            'content' => gmdate('Y'),
        ];

        foreach(config('emailAdditionalGlobalFields', []) as $fieldName => $data) {
            if(!empty($data['type']) && $data['type'] == 'image') {
                $content = url(Settings::getConfigValueUrl($data['settings_path']));
            } else {
                $content = Settings::getConfigValue($data['settings_path']);
            }
            $globalParams[] = [
                'name'    => $fieldName,
                'content' => $content,
            ];
        }
        return $globalParams;
    }

    public static function __callStatic($method, $parameters)
    {
        return resolve(static::class)->$method(...$parameters);
    }

}
